# B3IT - B3 Importing Tool

The main goal of this project is to extract data from 
B3's portal and
get relevant data of investor behavior

![panda_playground.gif](panda_playground.gif)

# Setup
1. Checkout project.
2. Download "movimentacao" reports from www.investidor.b3.com.br.
   1. Paste then in "/files" folder.
   2. You may need to change "files_to_extract" in "Extractor.py".
   3. Also download the "posicao" report from portal and put it in the same folder
2. ``` source venv/Scripts/activate ```
   2. Remember to deactivate virtualenv in pycharm>settings>terminal
3. ```pip install -r requirements.txt```
4. run main.py

# Goals:
step 1: get data directly from files without 
django ✔️.

step 2: use pandas to calculate ticker 
report ✔️.

step 2.1: cross summarized info from pandas with b3 
position report to estimate the data gaps with
position report ✔️.

step 3: use pandas to calculate dividends reports ✔️.

step 3.1: plot dividend growth chart ✔️.

step 3.2: segment dividend info by ticker ✔️.

step 4: cross reports info with yfinance lib to 
calculate ticker performance ✔️.

step 5: heatmap of ticker performance ✔️.

step 6: cross data between positon report and dividends report.

step 7: pizza graph of FII x market type.

FII report will cross the following info:
* top dividends
* top dividends on last 2 years
* position value
* status (div & pos >0, div + pos >0, div + pos < 0)

# Features
* Summarize operations.
* Summarize positions.
* Show dividend growth

# Notes
* Yes this is a fork, no It doesn't use any 
feature of the forked repo 😃.
* Although this calculates financial data based on B3 reports, they are NOT 100% accurate
because of B3 report limitations of past data.
* The yfinance has a limit of 2k requests per hour, don't be greedy!
* To run jupyter use ``` jupyter lab ```
