from copy import deepcopy
from Pandaren import andy_panda
from django.http import JsonResponse
import ftfy
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from unidecode import unidecode
from .response_docs import *


def fix_string(s):
    """
    this is a big workaround using ftfy and unidecode, that should be a better way to do this
    :param str s:
    :return:
    """
    return unidecode(ftfy.fix_text(s))


class GetHistogram(APIView):
    """
    API endpoint that return the invoice value month by month
    """
    @swagger_auto_schema(responses=GetHistogram_response)
    def get(self, request):
        data = andy_panda.get_histogram_for_view()
        histogram = {}
        for key, value in data.items():
            histogram[key] = {}
            histogram[key]['amount'] = round(value, 2)

        content = {
            'histogram': histogram
        }
        return JsonResponse(content)


class GetBillsFromDate(APIView):
    """
    API endpoint that return the bills in a given bill_date
    :param bill_date: date in year/month format, e.g.: 202212 means december of 2022

    """
    @swagger_auto_schema(responses=GetBillsFromDate_response)
    def get(self, request, bill_date):
        data = andy_panda.get_bills_from_date(bill_date).to_dict('records')
        bills = []
        default_bill = {
            'date': {},
            'category': {},
            'title': {},
            'amount': {},
        }
        i = 0
        for item in data:
            bills.append(deepcopy(default_bill))
            bills[i]['date'] = item['date']
            bills[i]['category'] = fix_string(item['category'])
            bills[i]['title'] = fix_string(item['title'])
            bills[i]['amount'] = round(item['amount'], 2)
            i += 1

        content = {
            'bills': bills
        }
        return JsonResponse(content)


class Report1(APIView):
    """
    Report 1
    This report ranks the purchases by category by month
    most_expensive_categories_by_month with or without outliers
    :param mode: 0 - no outliers; 1 - with outliers
    """
    @swagger_auto_schema(responses=Report1_response)
    def get(self, request, mode):
        data = []
        if mode == 0:
            data = andy_panda.most_expensive_categories_by_month_no_outliers()
        if mode == 1:
            data = andy_panda.most_expensive_categories_by_month()

        histogram = {}
        for bill_date in data:
            histogram[bill_date] = {}
            histogram[bill_date]['ranking'] = []
            for category, amount in data[bill_date].items():
                histogram[bill_date]['ranking'].append({
                    'category': fix_string(category),
                    'amount': round(amount, 2)
                })

        content = {
            'histogram': histogram
        }
        return JsonResponse(content)


class Report2(APIView):
    """
    Report 2
    This report gets the most expensive purchases by place by month
    get_most_expensive_purchases
    """
    @swagger_auto_schema(responses=Report2_response)
    def get(self, request):
        data = andy_panda.get_most_expensive_purchases()

        histogram = {}
        for bill_date in data:
            histogram[bill_date] = {}
            histogram[bill_date]['ranking'] = []
            for place, amount in data[bill_date].items():
                histogram[bill_date]['ranking'].append({
                    'place': fix_string(place),
                    'amount': round(amount, 2)
                })

        content = {
            'histogram': histogram
        }
        return JsonResponse(content)


class Report3(APIView):
    """
    Report 3
    This report gets the most expensive months
    get_most_expensive_months
    """
    @swagger_auto_schema(responses=Report3_response)
    def get(self, request):
        data = andy_panda.get_most_expensive_months()

        histogram = {}
        for bill_date, amount in data.items():
            histogram[bill_date] = round(amount, 2)

        content = {
            'histogram': histogram
        }
        return JsonResponse(content)


class Report4(APIView):
    """
    Report 4
    This report gets the most recurrent places by month
    get_most_recurrent
    """
    @swagger_auto_schema(responses=Report4_response)
    def get(self, request):
        data = andy_panda.get_most_recurrent()

        histogram = {}
        for bill_date in data:
            histogram[bill_date] = {}
            histogram[bill_date]['ranking'] = []
            places = data[bill_date].to_dict('split')['index']
            i = 0
            for row in data[bill_date].to_dict('records'):
                histogram[bill_date]['ranking'].append({
                    'place': fix_string(places[i]),
                    'count': row['amount']
                })
                i += 1

        content = {
            'histogram': histogram
        }
        return JsonResponse(content)
