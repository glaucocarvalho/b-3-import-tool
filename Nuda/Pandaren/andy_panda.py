import pandas as pd
import numpy as np
from scipy import stats
from DataXtractor.models import Bill

DATA_FRAME = pd.DataFrame(list(Bill.objects.all().values()))


def main():
    print(
        get_most_recurrent()
    )


def get_most_recurrent():
    bills_only = remove_payment(DATA_FRAME)
    bill_dates = get_histogram(bills_only)
    df_by_month = {}
    for item in bill_dates.items():
        date = item[0]
        df_by_month[date] = remove_payment(get_bills_from_date(date))

    expenses_occurrences_by_month = {}
    for month, df in df_by_month.items():
        expenses_occurrences = df.groupby(["title"]).count().sort_values("id", ascending=False).head(10)
        expenses_occurrences_by_month[month] = expenses_occurrences[
                                                    expenses_occurrences["id"] > 1
                                                ]  # Remove lone occurrences

    return expenses_occurrences_by_month


def get_most_expensive_months():
    return get_histogram(remove_payment(DATA_FRAME)).sort_values()


def get_most_expensive_purchases():
    bills_only = remove_payment(DATA_FRAME)
    bill_dates = get_histogram(bills_only)
    df_by_month = {}
    for item in bill_dates.items():
        date = item[0]
        df_by_month[date] = get_highest_50_pp(remove_payment(get_bills_from_date(date)))

    expenses_by_month = {}
    for month, df in df_by_month.items():
        expenses_by_title = df.groupby(["title"])["amount"].sum().sort_values()
        expenses_by_month[month] = expenses_by_title

    return expenses_by_month


def most_expensive_categories_by_month():
    bills_only = remove_payment(DATA_FRAME)
    bill_dates = get_histogram(bills_only)
    df_by_month = {}
    for item in bill_dates.items():
        date = item[0]
        df_by_month[date] = get_lowest_50_pp(remove_payment(get_bills_from_date(date)))

    expenses_by_category_by_month = {}
    for month, df in df_by_month.items():
        expenses_by_category = df.groupby(["category"])["amount"].sum().sort_values()
        expenses_by_category_by_month[month] = expenses_by_category

    return expenses_by_category_by_month


def most_expensive_categories_by_month_no_outliers():
    bills_only = remove_payment(DATA_FRAME)
    bill_dates = get_histogram(bills_only)
    df_by_month = {}
    for item in bill_dates.items():
        date = item[0]
        df_by_month[date] = get_lowest_50_pp(remove_outliers(remove_payment(get_bills_from_date(date)), "amount"))

    expenses_by_category_by_month = {}
    for month, df in df_by_month.items():
        expenses_by_category = df.groupby(["category"])["amount"].sum().sort_values()
        expenses_by_category_by_month[month] = expenses_by_category

    return expenses_by_category_by_month


def get_lowest_50_pp(df):
    return df[df["amount"] < df["amount"].mean()]


def get_highest_50_pp(df):
    return df[df["amount"] > df["amount"].mean()]


def general_outlier_analysis():
    bills_only = remove_payment(DATA_FRAME)
    print('base')
    describe_df(bills_only)
    df = remove_outliers(bills_only, "amount")
    print('without outliers')
    describe_df(df)
    df = get_outliers(bills_only, "amount")
    print('with outliers')
    describe_df(df)


def get_outliers(data_frame, column):
    return data_frame[(np.abs(stats.zscore(data_frame[column])) > 3)]


def remove_outliers(data_frame, column):
    return data_frame[(np.abs(stats.zscore(data_frame[column])) < 3)]


def remove_payment(df):
    return df[(df["amount"] > 0)]


def get_histogram_for_view():
    return get_histogram(remove_payment(DATA_FRAME))


def get_histogram(df):
    return df.groupby(["bill_date"])["amount"].sum()


def get_bills_from_date(bill_date):
    return DATA_FRAME[(DATA_FRAME["bill_date"] == bill_date)]


def describe():
    print(DATA_FRAME.describe())


def describe_df(df):
    print(df.describe())


def head():
    print(DATA_FRAME.head())


def columns():
    print(DATA_FRAME.columns.values)
