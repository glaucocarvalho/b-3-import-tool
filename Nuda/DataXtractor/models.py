from django.db import models


class Bill(models.Model):
    """
    This model stores each line of credit card bill csv
    """
    date = models.DateField()
    category = models.CharField(max_length=20)
    title = models.CharField(max_length=50)
    amount = models.FloatField()
    bill_date = models.CharField(max_length=6)  # date format: YYYYMM
    line_index = models.IntegerField(default=0)  # used to avoid duplicates of same billing

    def flush_bill_date(filename):
        build_date = filename[:len(filename) - 4].split('-')
        bill_date = build_date[1] + build_date[2]
        Bill.objects.filter(bill_date=bill_date).delete()


    def save_line(line, filename, index):
        fields = line.split(',')
        build_date = filename[:len(filename) - 4].split('-')
        b = Bill(
            date=fields[0],
            category=fields[1],
            title=fields[2],
            amount=fields[3],
            bill_date=build_date[1] + build_date[2],
            line_index=index,
        )
        b.save()


class InvoiceHistory(models.Model):
    """
    This model ensures that we do not use the same file two times
    """
    file_name = models.CharField(max_length=30)
    is_extracted = models.BooleanField(default=False)

    def get_file(file):
        return InvoiceHistory.objects.filter(file_name=file)

    def save_file(file):
        i = InvoiceHistory(file_name=file)
        i.save()
