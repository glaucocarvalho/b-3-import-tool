import os
from pathlib import Path

import pandas as pd

CURRENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
DATA_FOLDER = Path(CURRENT_FOLDER).resolve().joinpath("../../files/")


def get_data(file_type, year):
    if file_type == "mov":
        file_type = "movimentacao-"
    elif file_type == "neg":
        file_type = "negociacao-"
    else:
        print("ERROR: UNSPECTED TYPE FORMAT")
        return 1

    return pd.read_excel(DATA_FOLDER.joinpath(file_type + year + ".xlsx"), engine="openpyxl")


def get_staging_area_mov():
    files_to_extract = [
        "movimentacao-2019.xlsx",
        "movimentacao-2020.xlsx",
        "movimentacao-2021.xlsx",
        "movimentacao-2022.xlsx",
        "movimentacao-2023-01-06.xlsx",
        "movimentacao-2023-parcial.xlsx",
    ]

    staging_area = pd.DataFrame(columns=[
        "Entrada/Saída",
        "Data",
        "Movimentação",
        "Produto",
        "Instituição",
        "Quantidade",
        "Preço unitário",
        "Valor da Operação"
    ])

    for file in files_to_extract:
        df = pd.read_excel(DATA_FOLDER.joinpath(file), engine="openpyxl")
        staging_area = pd.concat([staging_area, df.iloc[::-1]])

    return staging_area.reset_index()


def get_staging_area_neg():
    files_to_extract = [
        "negociacao-2019.xlsx",
        "negociacao-2020.xlsx",
        "negociacao-2021.xlsx",
        "negociacao-2022.xlsx",
        "negociacao-2023-01-06.xlsx",
        "negociacao-2023-parcial.xlsx",
    ]

    staging_area = pd.DataFrame(columns=[
        "Data do Negócio",
        "Tipo de Movimentação",
        "Mercado",
        "Prazo / Vencimento",
        "Instituição",
        "Código de Negociação",
        "Quantidade",
        "Preço",
        "Valor",
        # mov columns
        # "Entrada/Saída",
        # "Data",
        # "Movimentação",
        # "Produto",
        # "Instituição",
        # "Quantidade",
        # "Preço unitário",
        # "Valor da Operação"
    ])

    for file in files_to_extract:
        df = pd.read_excel(DATA_FOLDER.joinpath(file), engine="openpyxl")
        staging_area = pd.concat([staging_area, df.iloc[::-1]])

    return staging_area.reset_index()


def get_posicao():
    latest_pos = "posicao-2023-07.xlsx"
    sheets = pd.ExcelFile(DATA_FOLDER.joinpath(latest_pos), engine="openpyxl")
    acoes = pd.read_excel(sheets, "Acoes")
    etf = pd.read_excel(sheets, "ETF")
    fdi = pd.read_excel(sheets, "Fundo de Investimento")
    td = pd.read_excel(sheets, "Tesouro Direto")

    df = pd.concat([acoes, etf, fdi])
    df = df.loc[df["Produto"].notna()]
    df = df.reset_index()
    return df


def get_test_data():
    return "TBD"


def get_data_from_server():
    return "TBD"
