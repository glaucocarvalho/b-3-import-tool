import pandas as pd


def get_players(df):
    players = pd.concat([df[['Jogador1']], df[['Jogador2']]], axis=1)
    players = players.melt(value_vars=['Jogador1', 'Jogador2'], ignore_index=True)
    players = players.drop_duplicates('value')['value']
    return players


def change_seats(df):
    print(df)
    df = df.loc[:, ['Jogador1', 'Boneco1', 'Jogador2', 'Boneco2', 'Resultado', 'Quantidade']] = \
        df.loc[:, ['Jogador2', 'Boneco2', 'Jogador1', 'Boneco1', 'Resultado', 'Quantidade']]
    df.loc[df['Resultado'] == 1, 'Resultado'] = 3  # temp value
    df.loc[df['Resultado'] == 2, 'Resultado'] = 1
    df.loc[df['Resultado'] == 3, 'Resultado'] = 2
    df = df.rename(
        columns={'Jogador1': 'Jogador2', 'Jogador2': 'Jogador1', 'Boneco1': 'Boneco2',  'Boneco2': 'Boneco1'})
    return df


def get_player_matches(df, player):
    matches = df[(df.Jogador1 == player)]
    matches_2 = df[(df.Jogador2 == player)]
    matches_2 = change_seats(matches_2)
    return pd.concat([matches, matches_2])


def get_player_stats(df, player):
    print(player)
    matches = get_player_matches(df, player)
    # print(matches.head())

    favourite_fighters = matches.groupby('Boneco1').sum('Quantidade').sort_values('Quantidade', ascending=False)
    total_victories = matches.loc[matches['Resultado'] == 1, ['Quantidade']].sum()
    total_defeats = matches.loc[matches['Resultado'] == 2, ['Quantidade']].sum()
    total_battles = matches['Quantidade'].sum()
    win_rate = total_victories * 100 / total_battles

    player = matches.groupby(['Jogador1', 'Boneco1']).size().sort_values(ascending=False)
    print(player.head())
    return 0


def get_stats_old(df):
    players = get_players(df)
    get_stats(df)
    for player in players:
        # stats = get_player_stats(df, player)
        break
    return 0


def sanitize_data(df):
    # columns
    # 'Jogador1', 'Boneco1', 'Jogador2', 'Boneco2', 'Resultado', 'Quantidade'
    # explode players
    df = pd.concat([df[['Jogador1', 'Boneco1', 'Resultado', 'Quantidade']],
                    df[['Jogador2', 'Boneco2', 'Resultado', 'Quantidade']]], axis=0)

    # get player 2
    player_2 = df[df['Jogador1'].isna()]
    # put player 2 in player 1 column
    player_2 = change_seats(player_2)
    # get player 1
    player_1 = df[df['Jogador2'].isna()]
    # merge players in a single dataset
    df = pd.concat([player_1[['Jogador1', 'Boneco1', 'Resultado', 'Quantidade']],
                    player_2[['Jogador1', 'Boneco1', 'Resultado', 'Quantidade']]], axis=0, ignore_index=True)

    # 1 = win; 2 = loss
    df = df.groupby(['Jogador1', 'Boneco1', 'Resultado']).sum('Quantidade')

    # player x fighter x result x quantity
    return df


def get_stats(df):
    # multiIndex
    # df['Quantidade'].index[0][0]
    # df -> dataset
    # df['Quantidade'] -> series
    # df['Quantidade'].index -> goupby groups
    # df['Quantidade'].index[0] -> first group
    # df['Quantidade'].index[0][0] -> first element of first group

    win_rate_global = 0
    win_rate_fighter = 0
    df = df.reset_index()
    # df['win_rate'] = df['Jogador1']
    # df['total_matches'] = df.apply(lambda x: get_total_player_matches(df, df['Jogador1']), axis=1)
    # total_matches = df.groupby('Jogador1')['Quantidade'].sum()
    df['total_matches'] = df.groupby(['Jogador1', 'Boneco1'])['Quantidade'].transform('sum')
    # df['total_matches_won'] = df[df['Resultado'] == 1].groupby(['Jogador1', 'Boneco1'])['Quantidade'].transform('sum')
    df['rate'] = df.apply(lambda x: get_rate(x['Quantidade'], x['total_matches']), axis=1)
    df['result_status'] = df['Resultado']
    df['Resultado'] = df['Resultado'].apply(set_victory_status)
    best_ranking = df[df['result_status'] == 1].sort_values('rate', ascending=False)
    worst_ranking = df[df['result_status'] == 2].sort_values('rate', ascending=False)
    ret = {
        'total': df,
        'best': best_ranking,
        'worst': worst_ranking
    }
    return ret


def get_rate(victories, total_matches):
    return 100 * victories / total_matches


def set_victory_status(status):
    return 'Vitória' if status == 1 else 'Derrota'


def double_players(df):
    df = pd.concat([df, change_seats(df)], axis=0, ignore_index=True)
    return df


def get_rivals(df):
    df = double_players(df)
    # df['Resultado'] = df['Resultado'].apply(set_victory_status)
    df = df.groupby(['Jogador1', 'Boneco1', 'Jogador2', 'Boneco2', 'Resultado']).sum('Quantidade')
    df = df.reset_index()
    df['total_matches'] = df.groupby(['Jogador1', 'Boneco1', 'Jogador2', 'Boneco2'])['Quantidade'].transform('sum')
    df['rate'] = df.apply(lambda x: get_rate(x['Quantidade'], x['total_matches']), axis=1)
    df['Resultado'] = df['Resultado'].apply(set_victory_status)
    print(df.head(50))

    return df
