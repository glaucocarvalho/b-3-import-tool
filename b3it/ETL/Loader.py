import numpy as np
import plotly.express as px
import plotly.figure_factory as ff
import plotly.graph_objects as go


def plot_best_player_percent(df, name):
    fig = px.bar(df, x="Jogador1", y="rate", color="Boneco1", barmode="group", title=name)
    fig.write_html('../html/best_player_percent.html', auto_open=True)


def plot_best_player_abs(df, name):
    fig = px.bar(df, x="Jogador1", y="Quantidade", color="Boneco1", barmode="group", title=name)
    fig.show()


def plot_favourite_fighters_sunburst(df, name):
    fig = px.sunburst(
        df,
        path=['Jogador1', 'Boneco1', 'Quantidade'],
        title=name
    )
    fig.show()


def plot_win_rate_sunburst(df, name):
    fig = px.sunburst(
        df,
        path=['Jogador1', 'Boneco1', 'rate'],
        title=name
    )
    fig.show()


def plot_rivals_sunburst(df, name):
    fig = px.sunburst(
        df,
        path=['Jogador1', 'Boneco1', 'Jogador2', 'Boneco2', 'Resultado', 'rate'],
        # path=['Jogador1', 'Jogador2', 'Resultado', 'rate'], # TODO: get summary of rivals JxJ only
        title=name
    )
    fig.write_html('../html/rivals_sunburst.html', auto_open=True)


def plot_tree_map(df, name):
    print(df.head(50))
    fig = px.treemap(df[df['result_status'] == 1],
                     path=[px.Constant(name), 'Jogador1', 'Boneco1'],
                     values='total_matches',
                     color='rate', hover_data=['rate'],
                     color_continuous_scale='RdBu',
                     color_continuous_midpoint=np.average(df['rate'], weights=df['Quantidade']))
    fig.update_layout(margin=dict(t=50, l=25, r=25, b=25))
    fig.write_html('../html/win_rate_tree_map.html', auto_open=True)


def plot_rivals_tree_map(df, name):
    print(df.head(50))
    fig = px.treemap(df,
                     path=[px.Constant(name), 'Jogador1', 'Jogador2', "Resultado"],
                     values='total_matches',
                     color='rate', hover_data=['rate'],
                     color_continuous_scale='RdBu',
                     color_continuous_midpoint=np.average(df['rate'], weights=df['Quantidade']))
    fig.update_layout(margin=dict(t=50, l=25, r=25, b=25))
    fig.write_html('../html/win_rate_tree_map.html', auto_open=True)


def plot_sankey(df, player, players):
    #needs to refactor this
    fig = go.Figure(data=[go.Sankey(
        node=dict(
            pad=15,
            thickness=20,
            line=dict(color="black", width=0.5),
            label=df['Jogador1'],
            color="blue"
        ),
        link=dict(
            source=[0, 1, 0, 2, 3, 3],  # indices correspond to labels, eg A1, A2, A1, B1, ...
            target=[2, 3, 3, 4, 4, 5],
            value=[8, 4, 2, 8, 4, 2]
        ))])
    fig.update_layout(title_text="Basic Sankey Diagram", font_size=10)
    fig.write_html('../html/sankey.html', auto_open=True)
