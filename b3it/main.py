import random

import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import yfinance as yf
from plotly.subplots import make_subplots

from ETL import Extractor as e

log_open_positions = False

# CNTO3 had missing sell events, I've probably sold it in the past.
# SBFG3 had missing buy events, I've probably brought it to the past.
# Actually CNTO3 turned into SBF3, that explains why the buy/sell orders where not found
# lets simply add then to the blacklist :)
black_list = [
    "CNTO3",
    "SBFG3"
]

mov_types = [
    "Compra",
    "Venda",
    "Dividendo",
    "Rendimento",
    "Juros Sobre Capital Próprio",
    "Juros",
    "Cisão",
    "Desdobro",
    "Transferência - Liquidação",
    "Atualização",
    "Direito de Subscrição",
    "Direitos de Subscrição - Não Exercido",
    "Transferência - Liquidação",
    "Cessão de Direitos",
    "Cessão de Direitos - Solicitada",
    "Direito de Subscrição",
]

passive_income = [
    "Juros Sobre Capital Próprio",
    "Dividendo",
    "Rendimento",
    "Dividendo",
    "Juros",
]

treasure_movs = [
    "Compra",
    "Venda"
]


def set_options():
    pd.set_option('display.max_columns', None)
    pd.set_option('display.expand_frame_repr', False)


def get_passive_income(df):
    return df[df["Movimentação"].isin(passive_income)]


def get_teasure(df):
    return df["Movimentação"].isin(treasure_movs)


def get_papers(df):
    return df[df["Movimentação"] == "Transferência - Liquidação"]


def drop_null_operations(df):
    df = df[df["Valor da Operação"] != "-"]
    df['vop'] = df["Valor da Operação"].astype(float)
    return df


def get_operation_summary(df):
    df = drop_null_operations(df)
    df = df.groupby(["Produto", "Entrada/Saída"]).agg({"Quantidade": "sum", "vop": "sum"})
    df["mp"] = get_medium_price(df, "vop", "Quantidade")
    return df


def get_negotiation_summary(df):
    df = df.groupby(["Código de Negociação", "Tipo de Movimentação"]).agg({"Quantidade": "sum", "Valor": "sum"})
    df["mp"] = get_medium_price(df, "Valor", "Quantidade")
    return df


def get_medium_price(df, value, qty):
    return df[value] / df[qty]


def set_op_index(op):
    return -1 if op == "Debito" else 1


def set_op_index_neg(op):
    return -1 if op == "Venda" else 1


def set_sigla(prod_name):
    ret = prod_name.split(" - ")[0]
    # This if handles the Fractionary pappers
    if ret[-1] == "F":
        ret = ret[:-1]
    return ret


def load_data(df):
    df["op_index"] = df["Entrada/Saída"].apply(set_op_index)
    df["op_vop"] = df["vop"] * df["op_index"]  # se for venda o valor da operação vai ficar negativo
    df["op_q"] = df["Quantidade"] * df["op_index"]  # se for venda a quanditade_operação vai ficar negativa
    df = df.groupby(["Produto"]).agg({"op_q": "sum", "op_vop": "sum"}).reset_index()
    return df


def load_data_neg(df):
    df["op_index"] = df["Tipo de Movimentação"].apply(set_op_index_neg)
    df["op_vop"] = df["Valor"] * df["op_index"]  # se for venda o valor da operação vai ficar negativo
    df["op_q"] = df["Quantidade"] * df["op_index"]  # se for venda a quanditade_operação vai ficar negativa
    df = df.groupby(["ticker"]).agg({"op_q": "sum", "op_vop": "sum"}).reset_index()
    return df


def fill_in_qty(df, ticker):
    df = pd.merge(df, ticker, how='left', left_on="ticker", right_on="Código de Negociação")
    return df


def set_qty_diff(df):
    df["qty_diff"] = df["Quantidade"] - df["op_q"]
    return df


def get_open_positions(df):
    # Here I have my closed_positions_summary,
    # with each paper, the quantity operated and the summary operation result
    # WARNING: note that values with negative quantities are incomplete importations from b3
    # That means that we have the sell operation logged but not the buy operation
    # This happens because of B3 limitations os past data.
    closed_positions_summary = df[df["op_q"] <= 0]
    closed_positions_summary = closed_positions_summary.reset_index()
    if log_open_positions:
        print("closed_positions_summary: ")
        print(closed_positions_summary)

    # Here I have my closed_positions_summary,
    # with each paper, the quantity operated and the summary operation result
    # only for papers that not have importation errors
    closed_positions_summary_without_errors = df[df["op_q"] == 0]
    closed_positions_summary_without_errors = closed_positions_summary_without_errors.reset_index()
    if log_open_positions:
        print("closed_positions_summary_without_errors: ")
        print(closed_positions_summary_without_errors)

    # Here I have my open_positions_summary,
    # with each paper, the actual paper quantity, operation value and medium price
    open_positions_summary = df.loc[(df["op_q"] > 0) & (df["op_vop"] > 0)]
    open_positions_summary["mp"] = get_medium_price(open_positions_summary, "op_vop", "op_q")
    open_positions_summary = open_positions_summary.reset_index()
    if log_open_positions:
        print("open_positions_summary: ")
        print(open_positions_summary)

    # Only FII data
    # open_fii = df.loc[(df["Produto"].str.contains("11")) & (df["op_q"] > 0) & (df["op_vop"] > 0)].reset_index()
    # print(open_fii)

    real_positions = e.get_posicao()
    real_pos_summ = real_positions.loc[:, ["Código de Negociação", "Quantidade"]]
    if log_open_positions:
        print("real_positions: ")
        print(real_positions)

    cross = pd.crosstab(real_positions["Código de Negociação"], real_positions["Instituição"])
    # print(cross)

    real_pos_dict = real_positions.to_dict()
    ticker = real_pos_dict["Código de Negociação"].values()
    qty = real_pos_dict["Quantidade"].values()
    pos_qty_dict = pd.DataFrame(ticker, qty)
    # print(pos_qty_dict)

    # use pos_qty_dict to fill the missing quantities in open_positions_summary
    open_positions = fill_in_qty(open_positions_summary, real_pos_summ)
    # open_positions = open_positions.loc[open_positions["Quantidade"].notna()]
    open_positions = set_qty_diff(open_positions)
    if log_open_positions:
        print("Open positions: ")
        print(open_positions)

    return open_positions


def get_anomalies(df):
    # One way to find anomalies out is to search for values with quantity operation != 0
    # Values == 0 means that the report is matching with position
    # Values > 0 means that there are missing buy orders on the extraction data
    # Values < 0 means that there are missing sell orders on extraction data
    return df[df["qty_diff"] != 0]


def check_ticker(ticker, open_pos_mov, open_pos_neg, anomalies_mov, anomalies_neg):
    print("Check ticker " + ticker + ":")
    print("open_pos_mov")
    print(open_pos_mov[open_pos_mov["ticker"] == ticker].reset_index())
    print("open_pos_neg")
    print(open_pos_neg[open_pos_neg["ticker"] == ticker].reset_index())
    print("anomalies_mov")
    print(anomalies_mov[anomalies_mov["ticker"] == ticker].reset_index())
    print("anomalies_neg")
    print(anomalies_neg[anomalies_neg["ticker"] == ticker].reset_index())


def position_report():
    base_df = e.get_staging_area_mov()
    neg_df = e.get_staging_area_neg()

    # Here I have a sorted df grouped by paper and operation, with quantity, value of operation and medium price
    operation_summary = get_operation_summary(get_papers(base_df))
    print("operation_summary: ")
    print(operation_summary)

    df = operation_summary.reset_index()
    df = load_data(df)
    df["ticker"] = df["Produto"].apply(set_sigla)

    print("Running get_open_positions for operations")
    open_pos_mov = get_open_positions(df)

    negotiation_summary = get_negotiation_summary(neg_df)
    print("negotiation_summary: ")
    print(negotiation_summary)

    # Negociation normalization
    neg_df = negotiation_summary.reset_index()
    neg_df["Produto"] = neg_df["Código de Negociação"]
    neg_df["ticker"] = neg_df["Produto"].apply(set_sigla)
    neg_df = load_data_neg(neg_df)
    print("Negociacoes: ")
    print(neg_df)

    print("Running get_open_positions for negociations")
    open_pos_neg = get_open_positions(neg_df)

    # Let's compare mov and neg dataframes to add missing values to the final df
    df = open_pos_mov.merge(open_pos_neg, on='ticker', how='outer', suffixes=('_mov', '_neg'))

    # drop blacklisted tickers
    df = df[~df['ticker'].isin(black_list)]

    # Values form paper rental (e.g.: PETRK316, PETRK327) are handled here
    df = df.loc[df["ticker"].str.len() < 7]

    # Well use these columns to check if the data calculations of both sources are matching
    df["op_q_diff"] = df["op_q_mov"] - df["op_q_neg"]  # Operation quantity
    df["op_vop_diff"] = df["op_vop_mov"] - df["op_vop_neg"]  # Value of operation
    df["mp_diff"] = df["mp_mov"] - df["mp_neg"]  # Medium price
    df["qty_diff"] = df["qty_diff_mov"] - df["qty_diff_neg"]  # Medium price

    columns_to_check = ["Produto", "ticker",
                        "op_q_mov", "op_vop_mov", "mp_mov", "qty_diff_mov",
                        "op_q_neg", "op_vop_neg", "mp_neg", "qty_diff_neg",
                        "op_q_diff", "op_q_diff", "op_q_diff", "op_q_diff"]

    # After all, "negociacao" report seems to be better for this position report.
    meaningful_columns = ["Produto", "ticker", "op_q_neg", "op_vop_neg", "mp_neg", "qty_diff_neg"]
    columns_rename = {
        "Produto": "Product",
        "ticker": "ticker",
        "op_q_neg": "op_q",
        "op_vop_neg": "op_vop",
        "mp_neg": "mp",
        "qty_diff_neg": "qty_diff",
    }

    # total_open_positions = df[columns_to_check]
    df = df[meaningful_columns].rename(columns=columns_rename)

    # Here we'll estimate the missing capital
    df["estimated_vop"] = (df["op_q"] + df["qty_diff"]) * df["mp"]
    total_open_positions = df
    print("total_open_positions: ")
    print(total_open_positions)

    total_anomalies = get_anomalies(total_open_positions).reset_index()
    print("total_anomalies: ")
    print(total_anomalies)

    # Prepare the data to use yfinance
    df = total_open_positions
    ticker_list = df.iloc[:, 1] + ".SA"
    ticker_list_srt = ' '.join(ticker_list)
    df["latest_value"] = 0

    tickers = yf.Tickers(ticker_list_srt)
    # The limit of yfinance is of 2k requests per hour
    for ticker in ticker_list:
        history = tickers.tickers[ticker].history(period="1d")
        # print(history)
        value_to_find = ticker[:-3]
        index = df[df['ticker'] == value_to_find].index[0]
        df.at[index, "latest_value"] = history["Close"]

    df["total_qty"] = df["op_q"] + df["qty_diff"]
    df["latest_vop"] = (df["op_q"] + df["qty_diff"]) * df["latest_value"]
    df["balance"] = df["latest_vop"] - df["estimated_vop"]
    df["balance %"] = df["balance"] * 100 / df["estimated_vop"]

    wallet = df
    print("Wallet: ")
    print(wallet)

    plot_ticker_performance(wallet)


def dividend_report():
    mov_df = e.get_staging_area_mov()
    df = mov_df
    pi_df = df[df["Movimentação"].isin(passive_income)]
    df = pi_df

    # Normalization of df
    df["value"] = df["Valor da Operação"]

    # Workaround for groupby the same columns
    df["date_y"] = pd.to_datetime(df["Data"])
    df["date_m"] = pd.to_datetime(df["Data"])
    df = df.sort_values(by="date_m", ascending=False)
    df = df.groupby([
        pd.Grouper(key="date_y", freq="Y"),
        pd.Grouper(key="date_m", freq="M")
    ]).agg({"Quantidade": sum, "value": sum})
    df = df.reset_index()
    dividends_by_month = df
    print("dividends_by_month")
    # print(dividends_by_month)

    df["date"] = df["date_m"]
    plot_bar_with_ma(dividends_by_month, "Dividends Growth by Month", 6)  # by month

    df = df.groupby([
        pd.Grouper(key="date", freq="Y")
    ]).agg({"Quantidade": sum, "value": sum}).reset_index()
    dividends_by_year = df
    print("dividends_by_year")
    # print(dividends_by_year)
    plot_bar_with_ma(dividends_by_year, "Dividends Growth by Year", 3)  # by year

    df = pi_df
    df["value"] = df["Valor da Operação"]
    df["ticker"] = df["Produto"].apply(set_sigla)
    df["date"] = pd.to_datetime(df["Data"])
    df = df.groupby([
        "ticker",
        pd.Grouper(key="date", freq="Y")
    ]).agg(
        {"Quantidade": sum, "value": sum}
    ).reset_index()
    df = df.sort_values(by=["date", "value"], ascending=[False, False])
    ticker_rank_by_year = df
    plot_div_rank_by_year(ticker_rank_by_year)

    df = df.groupby([
        "ticker"
    ]).agg(
        {"Quantidade": sum, "value": sum}
    ).reset_index()
    df = df.sort_values(by=["value"], ascending=[False])
    ticker_ranking_global = df
    plot_bar(ticker_ranking_global.head(10), "Top 10 Dividends", "ticker", "value")


def plot_bar(df, title, axis_x, axis_y):
    fig = px.bar(df,
                 x=axis_x, y=axis_y,
                 text_auto=".2s",
                 title=title)
    # This is how it works in jupyter
    # fig.show(renderer='iframe')
    fig.write_html("./html/bar" + str(random.random()) + ".html", auto_open=True)


def plot_bar_with_ma(df, title, window_size):
    df['MovingAverage'] = df['value'].rolling(window=window_size).mean()
    df["value"] = df["value"].apply(lambda x: round(x, 2))

    fig = go.Figure()

    fig.add_trace(
        go.Scatter(
            x=df["date"].values,
            y=df["MovingAverage"].values,
            name="MovingAverage: " + str(window_size),
        ))

    fig.add_trace(
        go.Bar(
            x=df["date"].values,
            y=df["value"].values,
            text=df["value"].values,
            name="Dividends",
        ))

    fig.update_layout(
        title=title
    )

    # fig.show()
    fig.write_html("./html/bwma" + str(random.random()) + ".html", auto_open=True)


def plot_div_rank_by_year(df):
    df["value"] = df["value"].apply(lambda x: round(x, 2))
    years_to_show = df.groupby([pd.Grouper(key="date", freq="Y")])\
        .count().sort_values(by=["date"], ascending=[False]).reset_index()
    years_to_show_int = int(years_to_show["ticker"].count())

    fig = make_subplots(rows=years_to_show_int, cols=1)
    row = 1

    for df_year in years_to_show["date"]:
        timestamp = pd.Timestamp(df_year)
        year = timestamp.year
        sub_df = df[df["date"].dt.year == year].head(10)
        fig.add_trace(
            go.Bar(
                x=sub_df["ticker"],
                y=sub_df["value"],
                text=sub_df["value"].values,
                name="Top 10 divs of " + str(year)),
            row=row, col=1)
        row += 1

    fig.update_layout(
        title="Ranking of dividends by year",
        height=500 * years_to_show_int
    )

    fig.write_html("./html/drby" + str(random.random()) + ".html", auto_open=True)


def plot_ticker_performance(df):
    fig = px.treemap(df,
                     path=['ticker'],
                     values='estimated_vop',
                     color='balance %', hover_data=["balance", "total_qty", "latest_vop"],
                     color_continuous_scale='RdBu',
                     color_continuous_midpoint=0)
    fig.update_layout(
        margin=dict(t=50, l=25, r=25, b=25),
        title="Ticker performance"
    )

    fig.write_html("./html/tp" + str(random.random()) + ".html", auto_open=True)


def main():
    set_options()
    position_report()
    dividend_report()


if __name__ == '__main__':
    main()
